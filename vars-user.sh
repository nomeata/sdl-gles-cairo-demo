# --- ideally this array is empty.
# --- for the sdl-gles-cairo app we need to install gtk2hs-buildtools in the
# normal (non-cross-compiled) package db (while the cross-compiled one will
# be installed automatically), because the cabal package for cairo has a
# custom setup which will look for it in the normal db first.

cabaldepsnormal=(
    gtk2hs-buildtools-0.13.4.0
)

# --- profunctors-5.3 causes free-5.1 and adjunctions-4.4 to be rebuilt; gives warning but works.
cabaldepscross="
    cabal  base-orphans-0.6
    cabal  $rootdir/hs-packages/transformers-compat-0.6.2
    cabal  tagged-0.8.5
    cabal  semigroups-0.18.3
    cabal  StateVar-1.1.1.1
    cabal  contravariant-1.5
    cabal  primitive-0.6.4.0
    cabal  vector-0.12.0.1
    cabal  zlib-0.6.2
    cabal  JuicyPixels-3.3
    custom distributive-0.6
    custom comonad-5.0.3
    custom th-abstraction-0.2.6.0
    custom bifunctors-5.5.3
    cabal  hashable-1.2.7.0
    cabal  $rootdir/hs-packages/unordered-containers-0.2.8.0
    custom semigroupoids-5.2.2
    cabal  transformers-base-0.4.5.2
    cabal  profunctors-5.3
    cabal  void-0.7.2
    cabal  exceptions-0.10.0
    cabal  free-5.1
    cabal  adjunctions-4.4
    cabal  fixed-0.2.1.1
    cabal  half-0.3
    cabal  OpenGLRaw-3.3.0.1
    cabal  GLURaw-2.0.0.4
    cabal  OpenGL-3.0.2.0
    cabal  cabal-doctest-1.0.4
    cabal  profunctors-5.2.2
    cabal  prelude-extras-0.4.0.3
    cabal  exceptions-0.10.0
    cabal  call-stack-0.1.0
    cabal  fail-4.9.0.0 noarchive
    cabal  kan-extensions-5.0.2
    cabal  $rootdir/hs-packages/parallel-3.1.0.1
    cabal  $rootdir/hs-packages/reflection-2.1
    cabal  bifunctors
    cabal  $rootdir/hs-packages/hashtables-1.2.2.1
    cabal  random-1.1
    cabal  $rootdir/hs-packages/gtk2hs-buildtools-0.13.3.1
    cabal  integer-logarithms-1.0.2.1
    cabal  scientific-0.3.5.2
    custom utf8-string-0.2
    custom cereal-0.5.5.0
    custom bytes-0.15.5
    custom cairo-0.13.4.2
    custom lens-4.16
    # cabal  sdl2-2.4.0.1
    custom linear-1.20.7
    cabal  xml-1.3.14
    cabal  attoparsec-0.13.2.2
    cabal  $rootdir/hs-packages/svg-tree-0.6.2.1
    cabal  loop-0.3.0
    cabal  $rootdir/hs-packages/matrix-0.3.5.0
    cabal  dlist-0.8.0.4
    cabal  $rootdir/hs-packages/wavefront-0.7.1.2
    cabal  nats-1.1.2 noarchive
    cabal  ObjectName-1.1.0.1
    cabal  Stack-0.3.2
    cabal  cond-0.4.1.1
    cabal  resourcet-1.2.1
    cabal  split-0.2.3.3
    cabal  mono-traversable-1.0.9.0
    cabal  vector-algorithms-0.7.0.1
    cabal  unliftio-core-0.1.1.0
    cabal  conduit-1.3.0.2
    cabal  old-locale-1.0.0.7
    cabal  time-locale-compat-0.1.1.4
    cabal  uuid-types-1.0.3
    cabal  base-compat-0.10.4
    cabal  aeson-1.4.0.0
    cabal  yaml-0.8.28

    cabal  base64-bytestring-1.0.0.1
    cabal  raw-strings-qq-1.1
    cabal  monad-loops-0.4.3

    cabal MonadRandom-0.5.1.1
    cabal base16-bytestring-0.1.1.6
    cabal colour-2.3.4
    cabal hex-text-0.1.0.0

    # --- sdl is by far the most time-consuming, and no one depends on it.
    cabal  sdl2-2.4.0.1
"

haskellpackagesuser=(
    "cabal $HOME/ac/sdl-cairo-demo-cat"
    "cabal $HOME/ac/mesh-obj-gles"
    "cabal $HOME/ac/sdl-gles-cairo-demo"
)

ghcwiredindeps=(
    array-0.5.2.0
    base-4.11.0.0
    binary-0.8.5.1
    bytestring-0.10.8.2
    containers-0.5.10.2
    deepseq-1.4.3.0
    directory-1.3.1.5
    filepath-1.4.1.2
    ghc-prim-0.5.2.0
    integer-simple-0.1.1.1
    mtl-2.2.2
    parsec-3.1.12
    pretty-1.1.3.5
    process-1.6.1.0
    stm-2.4.4.1
    template-haskell-2.13.0.0
    text-1.2.3.0
    time-1.8.0.2
    transformers-0.5.4.0
    unix-2.7.2.2
    ghc-boot-th-8.3
)

# --- path to a lib, in contrast to `ghcwiredindeps`, which searches for them.
# --- rts in particular has too many variations.
manualstaticarchives=(
    "$ghclibpath/rts/libHSrts.a"
)

targets=(
    armeabi-v7a
    # x86
)
