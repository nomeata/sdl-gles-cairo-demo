# SDL/Cairo/OpenGL app framework in Haskell for Android & desktop.
## Also demonstrates how to build Haskell mobile (Android) apps using [ghc-cross-compile](https://gitlab.com/alleycatcc/ghc-cross-compile) and [haskell-android-sdl](https://gitlab.com/alleycatcc/haskell-android-sdl).

----

## To run the desktop version:

    cd <some-project-dir>
    git clone https://gitlab.com/alleycatcc/sdl-gles-cairo.git
    git clone https://gitlab.com/alleycatcc/mesh-obj-gles.git
    git clone https://gitlab.com/alleycatcc/sdl-cairo-demo-cat.git
    git clone https://gitlab.com/alleycatcc/sdl-gles-cairo-demo.git
    for i in sdl-gles-cairo mesh-obj-gles sdl-cairo-demo-cat; do
        cd "$i"; git submodule update --init --recursive
        cabal install
        cd ..
    done
    cd sdl-gles-cairo-demo
    cabal configure --flags=executable
    cabal run

## To run on Android:

- Build the .apk

        cd <some-project-dir>
        git clone https://gitlab.com/alleycatcc/sdl-gles-cairo.git
        git clone https://gitlab.com/alleycatcc/mesh-obj-gles.git
        git clone https://gitlab.com/alleycatcc/sdl-cairo-demo-cat.git
        git clone https://gitlab.com/alleycatcc/sdl-gles-cairo-demo.git
        for i in sdl-gles-cairo mesh-obj-gles sdl-cairo-demo-cat sdl-gles-cairo-demo; do
            cd "$i"; git submodule update --init --recursive; cd ..
        done
        cd sdl-gles-cairo-demo
        bin/docker build
    
- Connect the device via USB. Make sure it's in 'developer mode'.
- Push the .apk

        docker run --privileged -it -v /dev/bus/usb:/dev/bus/usb sdl-gles-android:latest
    

    - If the device shows an ok/cancel dialog try to accept immediately.
    
- If all goes well the app should show up in the drawer with an icon of a fish.
- When you run it, it may complain about text relocations, but you should be able to bypass the warning and run it anyway.
    
