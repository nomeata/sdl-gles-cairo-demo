module Launch ( launch, launchWithArgs ) where

import SDL hiding (clear)
import Data.Function
import Control.Monad

import qualified SDLRunner

type Logger = String -> IO ()


launch :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> IO ()
launch x y = launch' x y []

launchWithArgs :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> [String] -> IO ()
launchWithArgs = launch'

handleEvent :: a -> IO ()
handleEvent _ = return ()

launch' :: (Logger, Logger, Logger) -> Maybe (Int, Int) -> [String] -> IO ()
launch' loggers@(info', warn', error') dimsMb args = SDLRunner.main
