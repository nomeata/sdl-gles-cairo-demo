#version 100
#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D texture;
uniform float ambientStrength;
uniform float specularStrength;

varying vec4 v_position;
varying vec2 v_texcoord;
varying vec4 v_normal;

varying float v_specularExp;
varying vec4 v_ambientColor;
varying vec4 v_diffuseColor;
varying vec4 v_specularColor;

vec4 viewPos  = vec4 (-0.0, -0.0, 10.0, 1.0);

struct light {
    float textureAlpha;
    float ambientStrength;
    float specularStrength;
    float specularExp;
    vec4 ambientColor;
    vec4 diffuseColor;
    vec4 specularColor;
    vec4 lightPos;
};

// ok to send entire struct as an arg?

vec4 get_lighting (vec4 viewDir, vec4 norm, light l)
{
    float textureAlpha = l.textureAlpha;
    vec4 lightDir = normalize (l.lightPos - v_position);
    float lightProj = dot (norm, lightDir);

    // xxx
    lightProj = abs (lightProj);

    vec4 ambient = l.ambientStrength * l.ambientColor;

    vec4 diffuse = max (lightProj, 0.0) * l.diffuseColor;

    vec4 reflectDir = reflect (-lightDir, norm);
    float reflectProj = dot (viewDir, reflectDir);
    float spec = pow (max (reflectProj, 0.0), l.specularExp);
    vec4 specular = l.specularStrength * l.specularColor * spec;

    ambient *= textureAlpha;
    specular *= textureAlpha * 0.0;
    diffuse *= textureAlpha * 0.0;

    return ambient + specular + diffuse;
}

    // --- use texture alpha as diffuse map
void main()
{
    vec4 tex = texture2D (texture, v_texcoord);
    // average x y and z to get 'alpha', ignoring w.
    float textureAlpha = (tex.x + tex.y + tex.z) / 3.0;

    vec4 diffuseColor = v_diffuseColor;

    light l = light (
        textureAlpha,
        ambientStrength,
        specularStrength,
        v_specularExp,
        v_ambientColor,
        diffuseColor,
        v_specularColor,
        vec4 (-10.0, -2.0, 3.0, 1.0)
    );

    vec4 norm = normalize (v_normal);
    norm.w = 0.0;

    vec4 viewDir = normalize (viewPos - v_position);

    vec4 lightTotal = vec4 (0.0, 0.0, 0.0, 0.0);
    lightTotal += get_lighting (viewDir, norm, l);

    // gl_FragColor = tex * lightTotal;
    gl_FragColor = lightTotal;

    // gl_FragColor.w = 1.0;
}
